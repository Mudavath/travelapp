user nginx;
worker_processes auto;
pid /run/nginx.pid;

events {
	worker_connections 768;
}

http {

	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;

  upstream django {
    server app_server:8000;
  }

	server {
	  listen 80 default_server;
	  server_name _;
	  return 301 https://$host$request_uri;
	}

  server {
		listen              443 ssl;
		server_name         www.shophere.com;
		ssl_certificate     /etc/nginx/SSLCertificate.crt;
		ssl_certificate_key /etc/nginx/SSLCertificate.key;

    location / {
	    proxy_pass http://django;
	    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	    proxy_set_header X-Forwarded-Proto https;
	    proxy_set_header Host $host;
	    proxy_redirect off;
    }

		location /static/ {
			alias /usr/src/app/static/;
		}

		location /media/ {
			alias /usr/src/app/media/;
		}
  }

	ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
	ssl_prefer_server_ciphers on;

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

	gzip on;
	gzip_disable "msie6";

}
