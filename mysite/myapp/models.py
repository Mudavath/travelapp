from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
import datetime

# Create your models here.

class User(AbstractUser):
    CLIENTUSER = 1
    MODERATOR = 2
    ROLE_CHOICES = (
        (CLIENTUSER, 'Clientuser'),
        (MODERATOR, 'Moderator'),
    )
    role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, blank=True, null=True)

    def __str__(self):
        return '{0} User'.format(self.username)

class Destination(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField()
    description = models.TextField()
    price = models.IntegerField()
    def __str__(self):
        return self.description

class UserSelection(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    destination = models.ForeignKey(Destination, on_delete=models.CASCADE)
    def get_userselection(self):
        return {
            'user': user,
            'destination': destination,
        }
