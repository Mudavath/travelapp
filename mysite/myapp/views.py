from django.shortcuts import redirect
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.http import Http404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.urls import reverse_lazy
from django.template import loader
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib import messages

from urllib.parse import urlencode
import requests
import json

from django.conf import settings
from .models import *
from .forms import *
from .roles import *

# Create your views here.

class IndexView(generic.ListView):
    template_name = 'myapp/index.html'
    context_object_name = 'destinations_list'
    def get_queryset(self):
        return Destination.objects.all()

class DetailView(LoginRequiredMixin, generic.DetailView):
    login_url = reverse_lazy('myapp:login')
    redirect_field_name = 'redirect_to'
    model = Destination
    template_name = 'myapp/detail.html'

class AdminPanelView(LoginRequiredMixin, UserPassesTestMixin, generic.TemplateView):
    login_url = reverse_lazy('myapp:login')
    redirect_field_name = 'redirect_to'
    template_name = 'myapp/site_panel.html'
    def test_func(self):
        user = self.request.user
        return user.is_superuser or user.role == 2

class UsersView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    login_url = reverse_lazy('myapp:login')
    redirect_field_name = 'redirect_to'
    permission_required = ('myapp.view_user')
    raise_exception = True
    template_name = 'myapp/view_users.html'
    context_object_name = 'user_list'
    def get_queryset(self):
        return User.objects.order_by('pk')

@login_required(login_url=reverse_lazy('myapp:login'), redirect_field_name='redirect_to')
@permission_required('myapp.add_user', raise_exception=True)
def create_user(request):
    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            user = form.save()
            if user.role == 2:
                user.groups.add(moderator_group)
            elif user.role == 1:
                user.groups.add(clientuser_group)
            messages.success(request, 'Account has been Created Successfully!!')
            return HttpResponseRedirect(reverse('myapp:view_users'))
    else:
        form = UserCreateForm()
    return render(request, 'myapp/create_user.html', {'form': form})

@login_required(login_url=reverse_lazy('myapp:login'), redirect_field_name='redirect_to')
@permission_required('myapp.delete_user', raise_exception=True)
def delete_user(request, pk):
    user = get_object_or_404(User, pk=pk)
    user.delete()
    messages.success(request, 'Account has been Deleted Successfully!!')
    return HttpResponseRedirect(reverse('myapp:view_users'))

class DestinationsView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    login_url = reverse_lazy('myapp:login')
    redirect_field_name = 'redirect_to'
    permission_required = ('myapp.view_destination')
    raise_exception = True
    template_name = 'myapp/view_destinations.html'
    context_object_name = 'destination_list'
    def get_queryset(self):
        return Destination.objects.order_by('pk')

@login_required(login_url=reverse_lazy('myapp:login'), redirect_field_name='redirect_to')
@permission_required('myapp.add_destination', raise_exception=True)
def create_destination(request):
    if request.method == 'POST':
        form = DestinationCreateForm(request.POST, request.FILES)
        if form.is_valid():
            destination = form.save()
            messages.success(request, 'Destination has been Created Successfully!!')
            return HttpResponseRedirect(reverse('myapp:view_destinations'))
    else:
        form = DestinationCreateForm()
    return render(request, 'myapp/create_destination.html', {'form': form})

@login_required(login_url=reverse_lazy('myapp:login'), redirect_field_name='redirect_to')
@permission_required('myapp.delete_destination', raise_exception=True)
def delete_destination(request, pk):
    destination = get_object_or_404(Destination, pk=pk)
    destination.delete()
    messages.success(request, 'Destination has been Deleted Successfully!!')
    return HttpResponseRedirect(reverse('myapp:view_destinations'))

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            #------reCAPTCHA Validation----------------#
            recaptcha_response = request.POST.get('g-recaptcha-response')
            url = 'https://www.google.com/recaptcha/api/siteverify'
            values = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            data = urlencode(values).encode()
            headers = {"Content-Type": "application/x-www-form-urlencoded"}
            response = requests.post(url, data=data, headers=headers)
            result = response.json()
            #-------End reCAPTCHA validation-------------#
            if result['success']:
                user = form.save()
                user.groups.add(clientuser_group)
                messages.success(request, 'Your Account has been Created Successfully!!')
                return redirect('myapp:login')
            else:
                messages.warning(request, 'Invalid reCAPTCHA. Please try again.')
    else:
        if request.user.is_authenticated:
            return redirect('myapp:index')
        else:
            form = UserRegisterForm()
    return render(request, 'myapp/register.html', {'form': form})

@login_required(login_url=reverse_lazy('myapp:login'), redirect_field_name='redirect_to')
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('myapp:change_password')
        else:
            messages.warning(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'myapp/change_password.html', { 'form': form })

@login_required(login_url=reverse_lazy('myapp:login'), redirect_field_name='redirect_to')
def select(request, pk):
    destination = get_object_or_404(Destination, pk=pk)
    userselection = UserSelection.objects.filter(user=request.user, destination=destination)
    if len(userselection):
        messages.warning(request, "You had already selected this destination.")
        return HttpResponseRedirect(reverse('myapp:profile'))
    else:
        userselection = UserSelection(user=request.user, destination=destination)
        userselection.save()
        messages.success(request, 'Your selection was successfully recorded!')
        return HttpResponseRedirect(reverse('myapp:profile'))

@login_required(login_url=reverse_lazy('myapp:login'), redirect_field_name='redirect_to')
def deselect(request, pk):
    userselection = get_object_or_404(UserSelection, pk=pk)
    userselection.delete()
    messages.success(request, 'Your selection was successfully recorded!')
    return HttpResponseRedirect(reverse('myapp:profile'))

@login_required(login_url=reverse_lazy('myapp:login'), redirect_field_name='redirect_to')
def profile(request):
    user = User.objects.get(pk=request.user.id)
    selections = UserSelection.objects.filter(user=user)
    return render(request, 'myapp/profile.html', {'selections': selections})
