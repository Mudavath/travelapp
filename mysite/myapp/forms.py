from django import forms
from .models import User

from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm

from .models import *

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'password1', 'password2']

    def clean(self):
        super(UserRegisterForm, self).clean()
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')

        if len(username) < 8:
            self._errors['username'] = self.error_class(['Minimum 8 characters required'])
        if User.objects.filter(username=username).exists():
            self._errors['username'] = self.error_class(['Username already taken.'])
        if User.objects.filter(email=email).exists():
            self._errors['email'] = self.error_class(['Email address already taken.'])

        return self.cleaned_data

class UserCreateForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'password1', 'password2', 'role']

    def clean(self):
        super(UserCreateForm, self).clean()
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')
        role = self.cleaned_data.get('role')

        if len(username) < 8:
            self._errors['username'] = self.error_class(['Minimum 8 characters required'])
        if User.objects.filter(username=username).exists():
            self._errors['username'] = self.error_class(['Username already taken.'])
        if User.objects.filter(email=email).exists():
            self._errors['email'] = self.error_class(['Email address already taken.'])
        if role == None:
            self._errors['role'] = self.error_class(['Select a role for this user.'])

        return self.cleaned_data

class DestinationCreateForm(ModelForm):
    class Meta:
        model = Destination
        fields = ['name', 'description', 'image', 'price']
