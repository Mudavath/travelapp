from django.urls import path, include
from django.urls import reverse_lazy
from django.contrib.auth import views as auth_views

from . import views


app_name = 'myapp'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/select/', views.select, name='select'),
    path('<int:pk>/deselect/', views.deselect, name='deselect'),
    path('admin-panel/', views.AdminPanelView.as_view(), name='admin_panel'),
    path('create-user/', views.create_user, name='create_user'),
    path('<int:pk>/delete-user/', views.delete_user, name='delete_user'),
    path('view-users/', views.UsersView.as_view(), name='view_users'),
    path('create-destination/', views.create_destination, name='create_destination'),
    path('<int:pk>/delete-destination/', views.delete_destination, name='delete_destination'),
    path('view-destinations/', views.DestinationsView.as_view(), name='view_destinations'),
    path('profile/', views.profile, name='profile'),
    path('register/', views.register, name='register'),
    path('change-password/', views.change_password, name='change_password'),
    path('login/', auth_views.LoginView.as_view(redirect_authenticated_user=True, template_name='myapp/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='myapp/logout.html'), name='logout'),
    path('password-reset/',
      auth_views.PasswordResetView.as_view(template_name='myapp/password_reset.html',
      success_url=reverse_lazy('myapp:password_reset_done'), email_template_name = 'myapp/password_reset_email.html'),
      name='password_reset'),
    path('password-reset/done/',
      auth_views.PasswordResetDoneView.as_view(template_name='myapp/password_reset_done.html'),
      name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/',
      auth_views.PasswordResetConfirmView.as_view(template_name='myapp/password_reset_confirm.html', success_url=reverse_lazy(
        'myapp:password_reset_complete')),
      name='password_reset_confirm'),
    path('password-reset-complete/',
      auth_views.PasswordResetCompleteView.as_view(template_name='myapp/password_reset_complete.html'),
      name='password_reset_complete'),
]
