# travelapp

Travel App - Developed for Jan 2020

Multirole Application

App Features:
1.  Add User
2.  Delete User
3.  View Users
4.  Add Destination
5.  Delete Destination
6.  View Destinations
7.  Select Destionation
8.  Deselect Destination
9.  User Login
10. Register User
11. Forgot Password - (Password Reset Mail Not Sent, Only Printed to Console)
12. Change Password
13. User Profile
14. Home/Index Page

Security Features:
1.  Argon2 Password Hashing
2.  Password Complexity - { size: 12, type: Alpha Numeric With Symbol }
3.  Security Headers - (X-XSS-Protection, X-Content-Type-Option, X-Frame-Options, Strict-Transport-Security)
4.  No credentials in source code
5.  Cookie Flags Set - (HTTPOnly, Secure)
6.  Proper Authentication (Session Management)
7.  Proper Authorization (Role Based Access Control)
8.  Custom User Input Validation
9.  Django ORM and Models used for DB interactions (No SQLi)
10. Django Templates used for HTML (No XSS)
11. Logging and Log files has been setup
12. Google reCAPTCHA used with User Registeration
13. SSL/TLS Certificates Installed

Environment File:
1.  .env (pipenv shell) - [DEBUG, SECRET_KEY, GOOGLE_RECAPTCHA_SECRET_KEY, LOG_FILE_PATH, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD]
2.  .env.app (App Container) - [DEBUG, SECRET_KEY, DB_ENGINE, DB_DATABASE, DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, GOOGLE_RECAPTCHA_SECRET_KEY, LOG_FILE_PATH, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD]
3.  .env.db (DB Container) - [POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_DB]